Mini-TT: minimalistic ΠΣ-prover
===============================

Publication
-----------

* From Semantics to Computer Science. Essays in Honour of Gilles Kahn. <a href="http://www.cse.chalmers.se/~bengt/papers/GKminiTT.pdf">Chapter 6. A simple type-theoretic language: Mini-TT</a>, 2009, Cambridge University Press, ISBN-978-0-51177-052-4, pp. 139-164. Editors: Yves Bertot, Gérard Huet, Jean-Jacques Lévy, Gordon Plotkin

Authors
-------

* Thierry Coquand, Yoshiki Kinoshita, Bengt Nordström, Makoto Takeyama
